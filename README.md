# README #

Docker image for neurojupyter!

### What is neurojupyter for? ###

* It is largely based on neuralensemble/simulation Docker image
* It has a however a Jupyter Lab interface (instead of ssh/XWindow)
* It includes NEST 2.14, NEURON 7.4, PyNN 0.9, the scientific Python stack, and Julia 0.6.2 !

### How do I get set up? ###

* Download the Dockerfile (and the julia_init.jl) and, from the download folder, simply invoke
* ```docker build -t neurojupyter .```
* Alternatively pull it from Docker Hub, by ```docker pull meekeee/neurojupyter``` 

### How do I launch it? ###

* I added two aliases in my ```.bash_profile```, so that starting and stopping the neurojupyter container is easy

```alias neurojupyter='docker run -d --name myneurojupyter -p 8888:8888 -v "~/:/opt/notebooks" neurojupyter'```
```alias qneurojupyter='docker stop myneurojupyter && docker rm myneurojupyter'```

* Note that the container will mount the current user home folder ```~/``` as ```/opt/notebooks```, for maximal flexibility
* Point your browser to ```localhost:8888``` and enjoy!