Pkg.init()
Pkg.update()

ENV["PYTHON"] = "/home/docker/env/neurosci/bin/python"
Pkg.add("PyCall")
Pkg.add("PyPlot")

ENV["JUPYTER"] = "/home/docker/env/neurosci/bin/jupyter"
Pkg.add("IJulia")
Pkg.add("Interact")
Pkg.add("Reactive")

Pkg.add("Plots")
Pkg.add("GR")
#Pkg.add("UnicodePlots")
Pkg.add("PlotlyJS")
#Pkg.add("Gadfly")
#Pkg.add("Winston")
Pkg.add("StatPlots")
Pkg.add("PlotRecipes")
Pkg.add("LaTeXStrings")


Pkg.add("DSP")
Pkg.add("Distributions")
Pkg.add("BenchmarkTools")
#Pkg.add("ProgressMeter")
Pkg.add("Colors")
Pkg.add("NumericExtensions")

#Pkg.add("ImageMagick")
#Pkg.add("Color")
#Pkg.add("AverageShiftedHistograms")

Pkg.build("ZMQ")
#Pkg.build()
#const SEPARATOR = "\n\n" * "#"^80 * "\n\n"
#for pkg in keys(Pkg.installed())
#   try
#     info("Compiling: $pkg")
#     eval(Expr(:toplevel, Expr(:using, Symbol(pkg))))
#     println(SEPARATOR)
#   catch err
#     warn("Unable to precompile: $pkg")
#     warn(err)
#     println(SEPARATOR)
#   end
#end